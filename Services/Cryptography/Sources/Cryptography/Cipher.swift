import Foundation

/**
  Service to apply cryptography algorithms
 */
public protocol Cipher {

    /**
            Compute hash of string
     */
    func calculateHash(of plaintText: String) -> String?

    
    /**
            Encrypts plain text with AES algo
     */
    func encrypt(_ plainText: String, with key: String) -> String?

    /**
            Decrypts previously encrypyed text with AES algo
     */
    func decrypt(_ encryptedText: String, with key: String) -> String?
    
}
