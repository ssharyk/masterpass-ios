import Foundation
import CryptoKit

@available(iOS 13.0, *)
public class CipherModule: Cipher {

    public init() {}
    
    public func calculateHash(of plaintText: String) -> String? {
        return Digest.sha(of: plaintText)
    }

    public func encrypt(_ plainText: String, with key: String) -> String? {
        guard let keyData = key.padding(toLength: 16, withPad: " ", startingAt: 0).data(using: .utf8) else { return nil }
        let symmetricKey = SymmetricKey(data: keyData)
        
        guard let plainTextData = plainText.data(using: .utf8) else { return nil }
        guard let cryptedBox = try? AES.GCM.seal(plainTextData, using: symmetricKey) else { return nil }
        guard let allCrypted = cryptedBox.combined else { return nil }
        return allCrypted.base64EncodedString()
    }

    public func decrypt(_ encryptedText: String, with key: String) -> String? {
        guard let encryptedData = Data.init(base64Encoded: encryptedText) else { return nil }
        guard let sealedBoxToOpen = try? AES.GCM.SealedBox(combined: encryptedData) else { return nil }
        
        guard let keyData = key.padding(toLength: 16, withPad: " ", startingAt: 0).data(using: .utf8) else { return nil }
        let symmetricKey = SymmetricKey(data: keyData)
        
        guard let decryptedData = try? AES.GCM.open(sealedBoxToOpen, using: symmetricKey) else { return nil }
        let decryptedString = String(data: decryptedData, encoding: .utf8)
        return decryptedString
    }

}
