import XCTest
@testable import Cryptography

struct HashFixture {
    let originString: String
    let expectedHash: String
}

final class DigestTests: ParameterizedTest {

    override class func createTestCases() -> [ParameterizedTest] {
        return self.testInvocations.map { DigestTests(invocation: $0) }
    }

    override class var fixtures: [Any] {
        get {
            return [
                HashFixture(originString: "", expectedHash: "da39a3ee5e6b4b0d3255bfef95601890afd80709"),

                HashFixture(originString: "123456", expectedHash: "7c4a8d09ca3762af61e59520943dc26494f8941b")
            ]
        }
    }

    // Don't run test method. Run test class instead.
    func testComputeHash() {
        guard let fixture = self.fixture as? HashFixture else {
            XCTFail()
            return
        }
        let actual = Digest.sha(of: fixture.originString)
        XCTAssertEqual(fixture.expectedHash, actual)
    }

}
