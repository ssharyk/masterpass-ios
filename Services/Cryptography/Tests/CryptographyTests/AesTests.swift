import XCTest
@testable import Cryptography

struct AesFixture {
    let plain: String
}

@available(iOS 13.0, *)
final class AesTests: ParameterizedTest {
    
    override class func createTestCases() -> [ParameterizedTest] {
        return self.testInvocations.map { AesTests(invocation: $0) }
    }
    
    override class var fixtures: [Any] {
        get {
            return [
                AesFixture(plain: "FeDcBa0987654321")
            ]
        }
    }
    
    static let password = "FeDcBa0987654321FeDcBa0987654321"

    func testCrypt() {
        let cipher = CipherModule()
        guard let fixture = self.fixture as? AesFixture else {
            XCTFail()
            return
        }
        guard let encrypted = cipher.encrypt(fixture.plain, with: Self.password) else {
            XCTFail()
            return
        }
        let actual = cipher.decrypt(encrypted, with: Self.password)
        XCTAssertEqual(fixture.plain, actual)
    }
    
}
