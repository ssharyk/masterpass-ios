import Foundation
import Component

public class PreferencesModule: PreferencesService {
    
    private static var KEY_MASTER_PASS_HASH = "KEY_MASTER_PASS_HASH"
    
    private let prefs = UserDefaults.standard
    
    public var masterPassHash: LiveData<DefaultSubscriber<String?>, String?> = LiveData()
    
    public init() {
        guard let cached = prefs.string(forKey: PreferencesModule.KEY_MASTER_PASS_HASH) else { return }
        setMasterPassHash(cached)
    }
    
    public func setMasterPassHash(_ hash: String) {
        prefs.set(hash, forKey: PreferencesModule.KEY_MASTER_PASS_HASH)
        masterPassHash.change(newValue: hash)
    }
    
}
