import Foundation
import Component

public protocol PreferencesService {

    var masterPassHash: LiveData<DefaultSubscriber<String?>, String?> { get }
    
    func setMasterPassHash(_ hash: String)

}
