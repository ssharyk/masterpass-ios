import Foundation
import CoreData
import Component

@objc(PasswordEntity)
public class PasswordEntity: NSManagedObject {
}

extension PasswordEntity: Then {}

extension PasswordEntity {
    @nonobjc public class func fetchRequest() -> NSFetchRequest<PasswordEntity> {
        return NSFetchRequest<PasswordEntity>(entityName: "PasswordEntity")
    }

    @NSManaged public var desription: String?
    @NSManaged public var id: UUID?
    @NSManaged public var link: String?
    @NSManaged public var password: String?
    @NSManaged public var timestamp: Date?
    @NSManaged public var title: String?
    
    public override class func description() -> String {
        "PasswordEntity"
    }
}

extension PasswordEntity {
    public func toPassword(decrypted plainPassword: String) -> Password {
        return Password(
            id: self.id!.uuidString,
            name: self.title ?? "N/A",
            plainPassword: plainPassword,
            link: self.link ?? "N/A",
            description: self.desription ?? "",
            lastUpdateTimestamp: Int(self.timestamp?.timeIntervalSince1970 ?? TimeInterval(0))
        )
    }
}
