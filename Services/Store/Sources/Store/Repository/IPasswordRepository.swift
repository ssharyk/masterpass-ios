public protocol IPasswordRepository {

    /**
     Получить пароль с иденификатором `id`
     - Parameter id: идентификатор пароля
     - Returns: Найденный пароль, или `nil`, если не найдено
     */
    func findPassword(by id: String) -> PasswordEntity?
    
    /**
     Загрузка всех паролей пользователя
     */
    func findPasswords() -> [PasswordEntity]
     
    /**
     Добавить новую или изменить существующую (по идентификатору) запись о пароле
     */
    func insertOrReplacePassword(
        id: String,
        title: String,
        link: String,
        encryptedPassword: String,
        description: String?
    ) -> PasswordEntity?
    
    /**
     Удалить пароль с указанным идентификатором
     */
    func deletePasswordBy(
        id: String
    ) -> Bool
    
}
