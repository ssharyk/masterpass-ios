import CoreData
import Component


@available(iOS 10.0, *)
open class PersistentContainer: NSPersistentContainer {
}

@available(iOS 10.0, *)
public class CoreDataDatabaseRepository : IPasswordRepository {
    
    private var persistentContainer: NSPersistentContainer = {
        let container = PersistentContainer(name: "Model")
        container.viewContext.automaticallyMergesChangesFromParent = true
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("CORE_DATA: Unresolved error \(error)")
            }
        })
        return container
    }()
    
    private var context: NSManagedObjectContext {
        return self.persistentContainer.viewContext
    }
    
    public init() { }
    
    public func findPassword(by id: String) -> PasswordEntity? {
        do {
            let fetchRequest = PasswordEntity.fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "id = %@", id)
            return try context.fetch(fetchRequest).first
        } catch {
            return nil
        }
    }
    
    public func findPasswords() -> [PasswordEntity] {
        let fetchRequest = PasswordEntity.fetchRequest()
        do { return try context.fetch(fetchRequest) }
        catch { return [] }
    }
    
    public func deletePasswordBy(id: String) -> Bool {
        do {
            if let existing = findPassword(by: id) {
                context.delete(existing)
                try context.save()
            }
            return true
        } catch {
            return false
        }
    }
    
    public func insertOrReplacePassword(id: String, title: String, link: String, encryptedPassword: String, description: String?) -> PasswordEntity? {
        let existing = id.isEmpty ? nil : self.findPassword(by: id)
        let record = existing == nil
            ? insertPassword(id: id, title: title, link: link, encryptedPassword: encryptedPassword, description: description)
            : update(existing!, id: id, title: title, link: link, encryptedPassword: encryptedPassword, description: description)
        do {
            try context.save()
            return record
        } catch {
            return nil
        }
    }
        
    private func insertPassword(id: String, title: String, link: String, encryptedPassword: String, description: String?) -> PasswordEntity? {
        let passwordEntity = PasswordEntity(context: context).then {
            $0.id = UUID(uuidString: id)
            $0.title = title
            $0.link = link
            $0.password = encryptedPassword
            $0.desription = description
            $0.timestamp = Date()
        }
        context.insert(passwordEntity)
        return passwordEntity
    }
    
    private func update(_ password: PasswordEntity, id: String, title: String, link: String, encryptedPassword: String, description: String?) -> PasswordEntity? {
        
        password.setValue(title, forKey: "title")
        password.setValue(link, forKey: "link")
        password.setValue(description, forKey: "desription")
        password.setValue(encryptedPassword, forKey: "password")
        return password
    }
}
