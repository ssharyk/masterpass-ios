import Foundation

public class RuntimeModule: RuntimeService {
    
    public static var NO_PASSWORD: String = ""
    
    public var masterPass: String = NO_PASSWORD
    
    public init() {}
    
}
