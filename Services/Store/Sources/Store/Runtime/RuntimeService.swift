import Foundation

public protocol RuntimeService {
    
    /**
        Мастер-пароль (в расшифрованном виде)
     */
    var masterPass: String { get set }
}
