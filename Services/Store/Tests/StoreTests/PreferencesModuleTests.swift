import Foundation
@testable import Store
import XCTest

class PreferencesModuleTests: XCTestCase {
    
    func test_whenPassHashChanged_thenObserversApplied() {
        
        var currentMasterPassHash: String? = nil
        
        let module = PreferencesModule()
        module.masterPassHash.observe { currentMasterPassHash = $0 }
        XCTAssertNil(currentMasterPassHash)
        
        let newHash = "1234567890"
        module.setMasterPassHash(newHash)
        XCTAssertEqual(newHash, module.masterPassHash.currentValue)
        XCTAssertEqual(newHash, currentMasterPassHash)
    }
    
}
