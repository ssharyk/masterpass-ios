# Store

Contains data of the app:
 
 * master password - hashed in the local storage and plain - only in runtime
 * saved passwords - in database only as encrypted strings
