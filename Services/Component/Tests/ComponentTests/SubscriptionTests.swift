import Foundation
import XCTest
@testable import Component

class SubscriptionTests: XCTestCase {
    
    class TestObserver: DefaultSubscriber<Int> {

        private(set) var actualValue: Int = -1

        init() {
            super.init(changesHandler: {(_: Int) in })
        }

        override func onValueChanged(newValue: Int) {
            self.actualValue = newValue
        }
    }
    
    // MARK: - Tests
    
    func test_whenInitialized_thenDefaultValues() {
        let publisher = LiveData<TestObserver, Int>()
        let subscriber = TestObserver()
        XCTAssertNil(publisher.currentValue)
        XCTAssertEqual(-1, subscriber.actualValue)
    }
    
    func test_whenValueChangedBeforeSubscription_thenObserverInvoked() {
        let publisher = LiveData<TestObserver, Int>()
        let subscriber = TestObserver()
        
        publisher.change(newValue: 2)
        XCTAssertEqual(2, publisher.currentValue)
        XCTAssertEqual(-1, subscriber.actualValue)

        publisher.observe(subscriber: subscriber)
        XCTAssertEqual(2, publisher.currentValue)
        XCTAssertEqual(2, subscriber.actualValue)
    }
    
    func test_whenValueChangedAfterSubscription_thenObserverInvoked() {
        let publisher = LiveData<TestObserver, Int>()
        let subscriber = TestObserver()
        
        publisher.observe(subscriber: subscriber)
        XCTAssertNil(publisher.currentValue)
        XCTAssertEqual(-1, subscriber.actualValue)
        
        publisher.change(newValue: 2)
        XCTAssertEqual(2, publisher.currentValue)
        XCTAssertEqual(2, subscriber.actualValue)
    }
    
}
