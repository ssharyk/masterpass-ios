/**
    Представляет собой подписчика на изменения
 */
public protocol Observer {
    
    associatedtype Value
    
    /**
        Метод обратного вызова, который срабатывает,
        когда издатель публикует новое значение для наблюдаемого элеменат
     */
    func onValueChanged(newValue: Value)
    
}
