public class DefaultSubscriber<T>: Observer {
    public typealias Value = T
    
    private var changesHandler: (T) -> Void
    
    init(changesHandler: @escaping (T) -> Void) {
        self.changesHandler = changesHandler
    }
    
    public func onValueChanged(newValue: T) {
        self.changesHandler(newValue)
    }
    
}
