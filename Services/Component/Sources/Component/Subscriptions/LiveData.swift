/**
 Упрощенный аналог Android Jetpack LiveData
 */
public class LiveData<S, T> : Observable where S: Observer, S.Value == T {   
    private var observers: [S] = []

    private(set) public var currentValue: T? = nil

    public init(_ initialValue: T? = nil) {
        self.currentValue = initialValue
    }
    
    public func observe(subscriber: S) {
        observers.append(subscriber)
        if let current = currentValue {
            subscriber.onValueChanged(newValue: current)
        }
    }
    
    public func observe(_ closure: @escaping (T) -> Void) {
        let subscriber = DefaultSubscriber<T>(changesHandler: closure)
        self.observe(subscriber: subscriber as! S)
    }
    
    public func change(newValue: T) {
        self.currentValue = newValue
        observers.forEach { $0.onValueChanged(newValue: newValue) }
    }
}
