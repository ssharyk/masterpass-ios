/**
    Издатель изменений
 */
public protocol Observable  {
    
    associatedtype Value
    associatedtype Subscriber where Subscriber:Observer, Subscriber.Value == Value
    
    /**
        Текущее значение наблюдаемого элемента
     */
    var currentValue: Value? { get }
    
    /**
        Добавить слушателя изменений
     */
    func observe(subscriber: Subscriber)
}
