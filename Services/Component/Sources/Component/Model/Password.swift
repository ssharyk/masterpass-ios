import Foundation

/**
 Пароль, готовый к отображению - должен храниться только в ОЗУ
 */
public struct Password {
    public let id: String
    public let name: String
    public let plainPassword: String
    public let link: String
    public let description: String
    public let lastUpdateTimestamp: Int
    
    public init(id: String, name: String, plainPassword: String, link: String, description: String?, lastUpdateTimestamp: Int) {
        self.id = id
        self.name = name
        self.plainPassword = plainPassword
        self.link = link
        self.description = description ?? ""
        self.lastUpdateTimestamp = lastUpdateTimestamp
    }
    
    public static let NO_ID = ""
    public static let EMPTY = Password(id: NO_ID, name: "", plainPassword: "", link: "", description: "", lastUpdateTimestamp: 0)
    
    public static func newId() -> String {
        return UUID.init().uuidString
    }
}


extension Password: Identifiable {
}

extension Password: Equatable, Hashable {
}

extension Password {
    public var timeFormatted: String {
        let d = Date(timeIntervalSince1970: TimeInterval(lastUpdateTimestamp))
        let df = DateFormatter()
        df.dateFormat = "dd-MM-yyyy HH:mm"
        return df.string(from: d)
    }
}
