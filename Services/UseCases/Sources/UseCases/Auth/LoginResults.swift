public enum LoginResult {
    /**
     Успешный вход
     */
    case success
    
    /**
     Пользователь уже авторизован
     */
    case reused
    
    /**
     Есть пользователь с таким логином, но пароль не совпадает
     */
    case invalidPassword
    
}
