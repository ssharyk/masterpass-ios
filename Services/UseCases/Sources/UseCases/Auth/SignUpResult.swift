public enum SignUpResult {
    case success
    
    /**
     Пароль слишком слабый
     */
    case weak
    
    /**
     Пока приложение работает в однопользовательском режиме, попытка регистрации нового ключа при наличии мастер-пароля недопустимаs
     */
    case reused
    
    /**
     Внутренняя ошибка шифрования
     */
    case cipherError

}
