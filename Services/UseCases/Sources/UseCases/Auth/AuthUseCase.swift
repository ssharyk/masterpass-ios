import Store
import Cryptography

public class AuthUseCase : AuthService {
    
    private let store: PreferencesService
    private let cipher: Cipher
    private var runtime: RuntimeService
    private let validator = PasswordValidator()
    
    public init(store: PreferencesService, cipher: Cipher, runtime: RuntimeService) {
        self.store = store
        self.cipher = cipher
        self.runtime = runtime
    }
    
    public var hasRecord: Bool {
        !(store.masterPassHash.currentValue ?? "")!.isEmpty
    }
    
    public func reset() {
        runtime.masterPass = ""
    }
    
    public func login(plainPassword: String) -> LoginResult {
        guard let hashed = store.masterPassHash.currentValue else { return .invalidPassword }
        guard runtime.masterPass == "" else { return .reused }
        guard cipher.calculateHash(of: plainPassword) == hashed else { return .invalidPassword }
        
        saveIntoRuntime(plainPassword)
        return .success
    }
    
    public func register(plainPassword: String) -> SignUpResult {
        guard runtime.masterPass == "" else { return .reused }
        guard (store.masterPassHash.currentValue ?? "") == "" else { return .reused }
        
        guard let hashed = cipher.calculateHash(of: plainPassword)
        else { return .cipherError }
        
        guard validator.resolveStrength(of: plainPassword) == .strong else { return .weak }

        store.setMasterPassHash(hashed)
        saveIntoRuntime(plainPassword)
        
        return .success
    }
    
    private func saveIntoRuntime(_ plainPassword: String) {
        runtime.masterPass = plainPassword
    }
    
}
