public protocol AuthService {
    
    var hasRecord: Bool { get }
    
    func reset()
    
    func login(plainPassword: String) -> LoginResult
    
    func register(plainPassword: String) -> SignUpResult

}
