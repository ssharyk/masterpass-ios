import Store
import Component
import Cryptography
import Foundation

public class PasswordManager: PasswordService {
    
    // MARK: - Fields and properties
    
    private let repository: IPasswordRepository
    private let runtime: RuntimeService
    private let cipher: Cipher
    
    private lazy var generator: PasswordGenerator = PasswordGenerator()
    
    public init(repo: IPasswordRepository, runtime: RuntimeService, cipher: Cipher) {
        self.repository = repo
        self.runtime = runtime
        self.cipher = cipher
    }
    
    // MARK: - API
    
    public func getAllPasswords() -> [Password] {
        return runtime.masterPass == RuntimeModule.NO_PASSWORD
            ? [] : repository.findPasswords().compactMap(fromDbPasswordTransformer)
    }
    
    public func getPasswordBy(id: String) -> Password? {
        if let p = repository.findPassword(by: id) {
            return fromDbPasswordTransformer(password: p)
        } else {
            return nil
        }
    }
    
    public func update(password: Password) -> PasswordUpdateResult {
        guard let encrypted = cipher.encrypt(password.plainPassword, with: runtime.masterPass) else { return .cryptographyError }
        
        let id = password.id.isEmpty ? UUID().uuidString : password.id
        let created = repository.insertOrReplacePassword(id: id, title: password.name, link: password.link, encryptedPassword: encrypted, description: password.description)
        guard created != nil else { return .storeError }
        return .success
    }
    
    public func generate(length: Int, options: [Options]) -> String {
        return generator.create(length: length, options: options)
    }
    
    public func delete(id: String) -> Bool {
        repository.deletePasswordBy(id: id)
    }
    
    // MARK: - Helpers
    
    private func fromDbPasswordTransformer(password it: PasswordEntity) -> Password? {
        guard let p = it.password else { return nil }
        if let plainPass = cipher.decrypt(p, with: runtime.masterPass) {
            return it.toPassword(decrypted: plainPass)
        } else {
            return nil
        }
    }
    
}
