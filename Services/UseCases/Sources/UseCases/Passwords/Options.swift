public enum Options {
  case lowerAndUpperCases
  case digits
  case specialCharacters
}
