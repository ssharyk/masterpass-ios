public class PasswordValidator {
    
    public enum PasswordStrength: Equatable {
        case weak(reasons: [Vulnerability])
        case medium(reason: Vulnerability)
        case strong
    }
    
    /**
     Причина, по которой пароль считается слабым
     */
    public enum Vulnerability {
        case length     // слишком короткий
        case wellKnown  // какой-то широко распространенный пароль (например, password или 123456)
        case simple  // не содержит спец. символов
    }
    
    static var MIN_LENGTH = 6
    private static var KNOWN = ["password", "123456", "qwerty"]
    
    public func resolveStrength(of password: String) -> PasswordStrength {
        var vulnerabilities: [Vulnerability] = []
        if password.count < PasswordValidator.MIN_LENGTH {
            vulnerabilities.append(.length)
        }
        if PasswordValidator.KNOWN.contains(password) {
            vulnerabilities.append(.wellKnown)
        }
        if password.allSatisfy({ ("a"..."z").contains($0.lowercased()) }) {
            vulnerabilities.append(.simple)
        }
        
        switch(vulnerabilities.count) {
        case 0: return .strong
        case 1: return .medium(reason: vulnerabilities[0])
        default: return .weak(reasons: vulnerabilities)
        }
    }
    
}
