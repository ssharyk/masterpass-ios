public enum PasswordUpdateResult {
    /**
     Пароль успешно сохранен
     */
    case success
    
    /**
     Не удалось зашифровать пароль перед сохранением
     */
    case cryptographyError
    
    /**
     Не удалось записать готовый пароль в хранилище
     */
    case storeError
}
