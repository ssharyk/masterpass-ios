import Foundation

class PasswordGenerator {
    
    private static var LETTERS = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]
    private static var DIGITS = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
    private static var SPECS = [".", ",", "_"]
    
    public func create(length: Int, options: [Options]) -> String {
        let charsCount = options.contains(.specialCharacters) ? length / 6 : 0
        let digitsCount = options.contains(.digits) ? length / 3 : 0
        let allLettersCount = length - charsCount - digitsCount
        let upperCaseCount = options.contains(.lowerAndUpperCases) ? allLettersCount / 2 : 0
        let lowerCaseCount = allLettersCount - upperCaseCount
        
        var rndString = ""
        for _ in 0..<lowerCaseCount { rndString += PasswordGenerator.LETTERS.randomElement()! }
        for _ in 0..<upperCaseCount { rndString += PasswordGenerator.LETTERS.randomElement()!.uppercased() }
        for _ in 0..<digitsCount { rndString += PasswordGenerator.DIGITS.randomElement()! }
        for _ in 0..<charsCount { rndString += PasswordGenerator.SPECS.randomElement()! }
        return String(rndString.shuffled())
    }
}
