import Component

public protocol PasswordService {
    
    func getAllPasswords() -> [Password]
    
    func getPasswordBy(id: String) -> Password?
    
    func update(password: Password) -> PasswordUpdateResult
    
    func delete(id: String) -> Bool
    
    func generate(length: Int, options: [Options]) -> String
}
