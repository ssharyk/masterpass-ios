# UseCases

Contains main business logic for operations:

 * Authorization - check master password and provide feedback for login/signup
 * Password management - read passwords, add new and modify existing
