import XCTest
import Cryptography
import Component
import Store
@testable import UseCases

@available(iOS 10.0, *)
final class PasswordsUseCasesTests: XCTestCase {
    
    // MARK: - Fields
    
    private let repo = CoreDataDatabaseRepository()
    private let runtime = RuntimeModule()
    
    // MARK: - Test setup/tearDown
    
    override func setUp() {
        func insert(id: String) {
            repo.insertOrReplacePassword(id: id, title: "p\(id)", link: "", encryptedPassword: FakePasswords.MASTER_PASS_HASH.rawValue, description: nil)
        }
        
        insert(id: FAKE_UUID)
        insert(id: FAKE_UUID_2)
    }
    
    // MARK: - Helpers
    
    private func createUseCase() -> PasswordService {
        runtime.masterPass = FakePasswords.MASTER_PASS_PLAIN.rawValue
        return PasswordManager(repo: repo, runtime: runtime, cipher: CipherModule())
    }
    
    private func createPassword(id: String) -> Password {
        return Password(id: String(id), name: "p\(id)", plainPassword: "password\(id)", link: "", description: nil, lastUpdateTimestamp: 0)
    }
    
    // MARK: - Getting
    
    func test_whenGetAll_thenReturnCollection() {
        let uc = createUseCase()
        XCTAssertArrayEqual(actual: uc.getAllPasswords(), expected: [
            createPassword(id: FAKE_UUID), createPassword(id: FAKE_UUID_2)
        ])
    }
 
    func test_whenGetByValidId_thenReturnInstance() {
        let uc = createUseCase()
        let actual = uc.getPasswordBy(id: FAKE_UUID)
        let expected = createPassword(id: FAKE_UUID)
        XCTAssertEqual(actual, expected)
    }
    
    func test_whenGetByInvalidId_thenReturnNil() {
        let uc = createUseCase()
        XCTAssertNil(uc.getPasswordBy(id: "???"))
    }
    
    // MARK: - Update
    
    func test_whenUpdateForExisting_thenUpdated() {
        let uc = createUseCase()
        
        XCTAssertEqual(uc.getPasswordBy(id: FAKE_UUID), createPassword(id: FAKE_UUID))
        
        let new = Password(id: FAKE_UUID, name: "p1", plainPassword: "password1Updated", link: "", description: nil, lastUpdateTimestamp: 0)
        let r = uc.update(password: new)
        XCTAssertEqual(uc.getPasswordBy(id: FAKE_UUID), new)
        XCTAssertTrue(r)
    }
    
    func test_whenUpdateForNew_thenCreated() {
        let uc = createUseCase()
        XCTAssertNil(uc.getPasswordBy(id: "3"))
        
        let new = Password(id: FAKE_UUID, name: "p3", plainPassword: "password3Updated", link: "", description: nil, lastUpdateTimestamp: 0)
        let r = uc.update(password: new)
        XCTAssertEqual(uc.getPasswordBy(id: "3"), new)
        XCTAssertTrue(r)
    }
}

extension Password: Equatable {
    public static func == (lhs: Password, rhs: Password) -> Bool {
        return lhs.id == rhs.id && lhs.name == rhs.name && lhs.plainPassword == rhs.plainPassword
    }
}
