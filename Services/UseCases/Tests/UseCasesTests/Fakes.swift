import Store
import Component
import Foundation

enum FakePasswords: String {
    case MASTER_PASS_PLAIN = "123456!."
    case MASTER_PASS_HASH = "9b822f7b0573d510cfb5f667ac3267e3fa3af3d5"
}

class FakeStore : PreferencesService {
    
    private var hash: String? = nil
    
    var masterPassHash: LiveData<DefaultSubscriber<String?>, String?> = LiveData()
    
    init(defaultHash: String) {
        self.hash = defaultHash
        self.masterPassHash.change(newValue: defaultHash)
    }
    
    func setMasterPassHash(_ hash: String) {
        self.hash = hash
        self.masterPassHash.change(newValue: hash)
    }
}


let FAKE_UUID = "E621E1F8-C36C-495A-93FC-0C247A3E6E5F"
let FAKE_UUID_2 = "E621E1F8-C36C-495A-93FC-0C247A3E6E2F"
