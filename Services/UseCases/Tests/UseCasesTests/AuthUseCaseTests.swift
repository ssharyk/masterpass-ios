import XCTest
import Cryptography
import Store
@testable import UseCases

final class AuthUseCasesTests: XCTestCase {

    private func createUseCase(defaultHash: String, runtime: RuntimeService = RuntimeModule()) -> AuthService {
        return AuthUseCase(
            store: FakeStore(defaultHash: defaultHash),
            cipher: CipherModule(),
            runtime: runtime
        )
    }
    
    // MARK: - Has active records
    
    func test_whenNoDataInStore_thenNoActiveRecords() {
        let useCase = createUseCase(defaultHash: "")
        XCTAssertEqual(false, useCase.hasRecord)
    }
    
    func test_whenHasDataInStore_thenThereAreActiveRecords() {
        let useCase = createUseCase(defaultHash: FakePasswords.MASTER_PASS_HASH.rawValue)
        XCTAssertEqual(true, useCase.hasRecord)
    }
    
    // MARK: - Login
    
    func test_whenPasswordValid_thenSuccess() {
        let runtime = RuntimeModule()
        let useCase = createUseCase(defaultHash: FakePasswords.MASTER_PASS_HASH.rawValue, runtime: runtime)
        XCTAssertEqual(.success, useCase.login(plainPassword: FakePasswords.MASTER_PASS_PLAIN.rawValue))
        XCTAssertEqual(FakePasswords.MASTER_PASS_PLAIN.rawValue, runtime.masterPass)
    }
    
    func test_whenPasswordInvalid_thenNotLogged() {
        let runtime = RuntimeModule()
        let useCase = createUseCase(defaultHash: FakePasswords.MASTER_PASS_HASH.rawValue, runtime: runtime)
        XCTAssertEqual(.invalidPassword, useCase.login(plainPassword: "abc"))
        XCTAssertEqual("", runtime.masterPass)
    }
    
    // MARK: - Register
    
    func test_whenPasswordValidAndNewUser_thenRegistered() {
        let runtime = RuntimeModule()
        let useCase = createUseCase(defaultHash: "", runtime: runtime)
        XCTAssertEqual(.success, useCase.register(plainPassword: FakePasswords.MASTER_PASS_PLAIN.rawValue))
        XCTAssertEqual(FakePasswords.MASTER_PASS_PLAIN.rawValue, runtime.masterPass)
    }
    
    func test_whenPasswordWhenHasActiveUserAlready_thenNotLogged() {
        let runtime = RuntimeModule()
        let useCase = createUseCase(defaultHash: FakePasswords.MASTER_PASS_HASH.rawValue, runtime: runtime)
        XCTAssertEqual(.reused, useCase.register(plainPassword: FakePasswords.MASTER_PASS_PLAIN.rawValue))
        XCTAssertEqual("", runtime.masterPass)
    }
    
    func test_whenPasswordWeak_thenNotRegistered() {
        let runtime = RuntimeModule()
        let useCase = createUseCase(defaultHash: "", runtime: runtime)
        XCTAssertEqual(.weak, useCase.register(plainPassword: "abc"))
        XCTAssertEqual("", runtime.masterPass)
    }
    
}
