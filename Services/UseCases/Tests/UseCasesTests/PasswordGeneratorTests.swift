import XCTest
@testable import UseCases

final class PasswordGeneratorTests: XCTestCase {
    
    private let generator = PasswordGenerator()
    
    func test_whenLengthConfigured_thenMinLengthProvided() {
        let actual = generator.create(length: 12, options: [])
        XCTAssertEqual(12, actual.count)
        XCTAssertEqual(actual.lowercased(), actual)
        XCTAssertTrue(actual.allSatisfy { $0.isLetter })
    }
    
    func test_whenOptionsConfigured_thenCharactersProvided() {
        let actual = generator.create(length: 12, options: [.lowerAndUpperCases, .digits, .specialCharacters])
        XCTAssertEqual(12, actual.count)
        XCTAssertNotEqual(actual.lowercased(), actual)
        XCTAssertTrue(actual.contains(where: {$0.isNumber}))
        XCTAssertTrue(actual.contains(where: {$0 == "." || $0 == "_" || $0 == "," }))
    }
    
}
