import XCTest
@testable import UseCases

final class PasswordValidatorTests: XCTestCase {
    
    private let validator = PasswordValidator()
    
    func test_whenEmptyPassword_thenWeak() {
        let actual = validator.resolveStrength(of: "")
        XCTAssertEqual(.weak(reasons: [.length, .simple]), actual)
    }
 
    func test_whenShortPassword_thenWeak() {
        let actual = validator.resolveStrength(of: "aqsw")
        XCTAssertEqual(.weak(reasons: [.length, .simple]), actual)
    }
    
    func test_whenKnownPassword_thenWeak() {
        let actual = validator.resolveStrength(of: "password")
        XCTAssertEqual(.weak(reasons: [.wellKnown, .simple]), actual)
    }
    
    func test_whenNoSpecCharsPassword_thenWeak() {
        let actual = validator.resolveStrength(of: "zpaoisdiuv")
        XCTAssertEqual(.medium(reason: .simple), actual)
    }
    
    func test_whenShortPasswordWithChars_thenWeak() {
        let actual = validator.resolveStrength(of: "z!@#")
        XCTAssertEqual(.medium(reason: .length), actual)
    }
    
    func test_whenGoodPassword_thenStrong() {
        let actual = validator.resolveStrength(of: "kjd654%bdlds")
        XCTAssertEqual(.strong, actual)
    }
    
}
