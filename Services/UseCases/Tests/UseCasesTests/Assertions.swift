import XCTest

extension XCTestCase {
    func XCTAssertArrayEqual<T: Equatable>(actual: [T], expected: [T], onFail: @autoclosure () -> String = "") {
        
        let lenA = actual.count
        let lenE = expected.count
        XCTAssertEqual(lenA, lenE, "Lengths are different: \(lenA) != \(lenE)")
        
        for i in 0..<lenA {
            XCTAssertEqual(actual[i], expected[i], "Different items at \(i): expected \(expected[i]) but was \(actual[i]): \(onFail())")
        }
    }
}
