import SwiftUI

@main
struct MasterPassApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
