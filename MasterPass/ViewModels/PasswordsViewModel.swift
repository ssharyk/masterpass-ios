import UseCases
import Component
import Combine

class PasswordsViewModel: BaseViewModel {
    
    enum PasswordField {
        case resourceName, resourceLink, plainPassword
        case cipher, store
    }
    
    static let MIN_PASSWORD_LENGTH = 12
    
    private let passwordsUseCase: PasswordService
    
    @Published private(set) var passwords: [Password] = []
    
    subscript(_ passwordId: String) -> Password {
        return passwordId == Password.NO_ID
            ? .EMPTY
            : passwordsUseCase.getPasswordBy(id: passwordId) ?? .EMPTY
    }
    
    init(passwordsUseCase: PasswordService) {
        self.passwordsUseCase = passwordsUseCase
        super.init()
        invalidate()
    }
    
    
    private func invalidate() {
        passwords = passwordsUseCase.getAllPasswords()
    }
    
    // MARK: - Save (insert new or update existing)
    
    func generateNew(length: Int, withDigits: Bool, withSpecs: Bool, withTwoCases: Bool) -> String {
        var options: [Options] = []
        if withDigits { options.append(.digits) }
        if withSpecs { options.append(.specialCharacters) }
        if withTwoCases { options.append(.lowerAndUpperCases) }
        return passwordsUseCase.generate(length: length, options: options)
    }
    
    func save(id: String, name: String, link: String, description: String, password: String) -> [PasswordField] {
        let validation = validatePasswordFields(name, link, password)
        guard validation.count == 0 else { return validation }
        let selectedPassword = Password(id: id, name: name, plainPassword: password, link: link, description: description, lastUpdateTimestamp: 0)
        let saved = passwordsUseCase.update(password: selectedPassword)
        switch(saved) {
            case .success: invalidate(); return []
            case .cryptographyError: return [.cipher]
            case .storeError: return [.store]
        }
    }
    
    private func validatePasswordFields(_ name: String, _ link: String, _ password: String) -> [PasswordField] {
        var validation = [PasswordField]()
        if name.isEmpty { validation.append(.resourceName) }
        if link.isEmpty { validation.append(.resourceLink) }
        if password.isEmpty { validation.append(.plainPassword) }
        return validation
    }
    
    // MARK: - Delete
    
    func deleteBy(id: String) -> Bool {
        let deleted = passwordsUseCase.delete(id: id)
        if (deleted) {
            invalidate()
        }
        return deleted
    }
    
}
