import UseCases
import Store
import Cryptography
import Component

class ViewModelsFactory {
    
    static let shared = ViewModelsFactory()
    
    private lazy var store: PreferencesService = PreferencesModule()
    private lazy var cipher: Cipher = CipherModule()
    private lazy var runtime: RuntimeService = RuntimeModule()
    private lazy var repository: IPasswordRepository = CoreDataDatabaseRepository()
    
    private lazy var authUseCase = AuthUseCase(store: store, cipher: cipher, runtime: runtime)
    private lazy var passwordsUseCase = PasswordManager(repo: repository, runtime: runtime, cipher: cipher)
    
    private var viewModels: [Router.Screen: BaseViewModel] = [:]

    func createViewModel<VM: BaseViewModel>(for screen: Router.Screen) -> VM {
        let vm = viewModels[screen] ?? createViewModelInstance(for: screen)
        register(vm, for: screen)
        return vm as! VM
    }
    
    private func createViewModelInstance<VM: BaseViewModel>(for screen: Router.Screen) -> VM {
        switch(screen) {
        case .auth: return AuthViewModel(authUseCase: authUseCase) as! VM
        case .passwordsList, .passwordDetails: return PasswordsViewModel(passwordsUseCase: passwordsUseCase) as! VM
        }
    }
    
    private func register<VM: BaseViewModel>(_ vm: VM, for screen: Router.Screen) {
        viewModels[screen] = vm
    }
}
