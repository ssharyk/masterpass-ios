import UseCases
import Combine

class AuthViewModel: BaseViewModel {
    
    @Published var error: String? = nil
    
    private let authUseCase: AuthService
    private let router = Router.shared
    
    init(authUseCase: AuthService) {
        self.authUseCase = authUseCase
    }
    
    func attemptEnter(login: String, password: String) -> Bool {
        if authUseCase.hasRecord {
            return tryLogin(password)
        } else {
            return tryRegister(password)
        }
    }
    
    func reset() {
        authUseCase.reset()
    }
    
    private func tryLogin(_ password: String) -> Bool {
        let loginResult = authUseCase.login(plainPassword: password)
        switch loginResult {
        case .success:
            error = nil
            router.onLogged()
            return true
        case .reused:
            error = "auth_error_reused"
        case .invalidPassword:
            error = "auth_error_incorrect"
        }
        return false
    }
    
    private func tryRegister(_ password: String) -> Bool {
        let signUpResult = authUseCase.register(plainPassword: password)
        switch signUpResult {
        case .success:
            error = nil
            router.onLogged()
            return true
        case .weak:
            error = "auth_error_too_weak"
        case .reused:
            error = "auth_error_reused"
        case .cipherError:
            error = "auth_error_not_encrypted"
        }
        return false
    }
    
}
