import SwiftUI

struct TintModifier: ViewModifier {
    
    let color: Color
    
    func body(content: Content) -> some View {
        if #available(iOS 15.0, *) {
            content.tint(color)
        } else {
            content
        }
    }
    
}



extension View {
    func tintColor(_ color: Color) -> some View {
        self.modifier(TintModifier(color: color))
    }
}
