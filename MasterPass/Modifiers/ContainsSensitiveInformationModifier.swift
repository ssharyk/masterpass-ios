import SwiftUI

struct ContainsSensitiveInformationModifier: ViewModifier {
    
    @ObservedObject var router: Router = .shared
    @Environment(\.scenePhase) var scenePhase: ScenePhase
    
    func body(content: Content) -> some View {
        if #available(iOS 15.0, *) {
            content
                .privacySensitive()
                .onChange(of: scenePhase, perform: onLifecycleStateChanged)
        } else {
            content
                .onChange(of: scenePhase, perform: onLifecycleStateChanged)
        }
    }
    
    private func onLifecycleStateChanged(_ newPhase: ScenePhase) {
        if scenePhase != .active {
            router.onBackground()
            (ViewModelsFactory.shared.createViewModel(for: .auth) as AuthViewModel).reset()
        }
    }
    
}

extension View {
    func containsSensitiveInformation() -> some View {
        self.modifier(ContainsSensitiveInformationModifier())
    }
}

struct ContainsSensiitivInformationModifier_Previews: PreviewProvider {
    static var previews: some View {
        Text("Hello, world!")
            .containsSensitiveInformation()
    }
}
