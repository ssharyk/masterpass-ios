import SwiftUI

struct ContentView: View {
    
    @ObservedObject var router: Router = .shared
    
    @ViewBuilder
    var body: some View {
        switch router.currentScreen {
        case .auth:
            AuthScreen(viewModel: ViewModelsFactory.shared.createViewModel(for: .auth))
        case .passwordsList:
            PasswordsListScreen(viewModel: ViewModelsFactory.shared.createViewModel(for: .passwordsList))
                .containsSensitiveInformation()
        case .passwordDetails(let password):
            let vm = ViewModelsFactory.shared.createViewModel(for: .passwordsList) as PasswordsViewModel
            SinglePasswordScreen(viewModel: vm, password: password)
                .containsSensitiveInformation()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
