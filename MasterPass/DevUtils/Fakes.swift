import UseCases
import Component

class FakePasswordsUseCase: PasswordService {
    private var passwords: [String: Password] = [:]
    
    private static func createPassword(id: String) -> Password {
        return Password(id: String(id), name: "p\(id)", plainPassword: "password\(id)", link: "", description: "Some text for password \(id)", lastUpdateTimestamp: 0)
    }
    
    init(capacity: Int) {
        guard capacity > 0 else { return }
        for i in 1...capacity {
            let id = "p\(i)"
            passwords[id] = FakePasswordsUseCase.createPassword(id: id)
        }
    }
    
    
    func getAllPasswords() -> [Password] { passwords.values.map { $0 } }
    
    func getPasswordBy(id: String) -> Password? { passwords[id] }
    
    func update(password: Password) -> PasswordUpdateResult {
        passwords[password.id] = password
        return .success
    }
    
    func delete(id: String) -> Bool {
        passwords[id] = nil
        return true
    }
    
    func generate(length: Int, options: [Options]) -> String {
        let newP = FakePasswordsUseCase.createPassword(id: "")
        passwords[newP.id] = newP
        return newP.plainPassword
    }
    
}
