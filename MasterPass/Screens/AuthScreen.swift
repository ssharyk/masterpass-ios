import SwiftUI
import Combine

struct AuthScreen: View {
    
    @State var login = ""
    @State var password = ""
    
    @ObservedObject var viewModel: AuthViewModel
    
    var body: some View {
        VStack {
            Spacer()
            /*
            TextField("Login", text: $login) {}
                .padding()
                .border(.purple, width: CGFloat(3))
            */
            SecureField(LocalizedStringKey("auth_password_hint"), text: $password)
                .padding()
                .border(Constants.BORDER_COLOR, width: CGFloat(3))
            errorTextView
                .textCase(.uppercase)
                .foregroundColor(Color.red)
                .font(.headline.weight(.bold))
                .isHidden((viewModel.error ?? "").isEmpty)
                
            
            Spacer()
            Button(LocalizedStringKey("auth_enter")) {
                if viewModel.attemptEnter(login: login, password: password) {
                    password = ""
                    login = ""
                }
            }
                .alignmentGuide(.bottom, computeValue: { _ in 0 })
                .padding()
                .foregroundColor(Constants.FOREGROUND_COLOR)
        }
        .padding()
        .alignmentGuide(HorizontalAlignment.center, computeValue: { _ in 0 })
        .alignmentGuide(VerticalAlignment.center, computeValue: { _ in 0 })
    }
    
    private var errorTextView: Text {
        if let errMessageKey = viewModel.error {
            return Text(LocalizedStringKey(errMessageKey))
        } else {
            return Text("")
        }
    }
}

struct AuthScreen_Previews: PreviewProvider {
    static var previews: some View {
        AuthScreen(viewModel: ViewModelsFactory.shared.createViewModel(for: .auth))
            .environment(\.locale, .init(identifier: "ru"))
    }
}
