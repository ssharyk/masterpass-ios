import Combine
import Component

class Router : ObservableObject{
    
    enum Screen: Equatable, Hashable {
        case auth
        case passwordsList
        case passwordDetails(password: Password)
    }
    
    // MARK: - Singleton
    
    private init() { }
    static var shared: Router = Router()
    
    // MARK: - Actual navigation toggles
    
    /// экран, который отображается в настоящий момент
    @Published private(set) var currentScreen: Screen = .auth
 
    private var navigationStack: [Screen] = [.auth]
    
    /// экран, на котором был пользователь, когда приложение ушло в фон
    private var previousScreen: Router.Screen = .auth
    
    func onLogged() {
        move(to: previousScreen == .auth ? .passwordsList : previousScreen)
    }
    
    func toDetails(of password: Password) {
        self.move(to: .passwordDetails(password: password))
    }
    
    func onBackground() {
        previousScreen = currentScreen
        navigationStack = []
        currentScreen = .auth
    }
    
    func up() {
        guard !navigationStack.isEmpty else { return }
        navigationStack.removeLast()
        currentScreen = navigationStack.last ?? .auth
    }
    
    private func move(to screen: Screen) {
        self.currentScreen = screen
        navigationStack.append(screen)
    }
    
}
