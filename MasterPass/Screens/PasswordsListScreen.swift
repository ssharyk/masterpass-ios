import SwiftUI
import UseCases
import Component

struct PasswordsListScreen: View {
    
    @ObservedObject var viewModel: PasswordsViewModel
    
    @State private var showErrorToast = false
    @State private var errorMessage = ""
    
    
    @State var currentUserInteractionCellID: String?
    
    var body: some View {
        VStack {
            Toolbar(LocalizedStringKey("all_passwords_toolbar"), onBackPressed: nil) {
                Image(systemName: "plus")
                    .onTapGesture {
                        Router.shared.toDetails(of: Password.EMPTY)
                    }
            }
            if viewModel.passwords.count == 0 {
                VStack(alignment: .center, spacing: 0) {
                    Spacer()
                    Text(LocalizedStringKey("all_passwords_no_one_title"))
                        .padding()
                    
                    Button(LocalizedStringKey("all_passwords_no_one_button")) {
                        Router.shared.toDetails(of: Password.EMPTY)
                    }.foregroundColor(Constants.FOREGROUND_COLOR)
                        .padding()
                    Spacer()
                }
            } else {
                GeometryReader { proxy in
                    ScrollView {
                        LazyVStack {
                            ForEach(viewModel.passwords, id: \.id) { password in
                                PasswordListItemSwipeWrapper(availableWidth: proxy.size.width - 20, currentUserInteractionCellID: $currentUserInteractionCellID, password: password, deletionCallback: {
                                    viewModel.deleteBy(id: password.id)
                                })
                                    .toast(message: LocalizedStringKey(errorMessage), isShowing: $showErrorToast, duration: Toast.short)
                            }
                        }
                    }
                }
            }
        }
    }
}


struct PasswordListItemSwipeWrapper: View {
    
    var availableWidth: CGFloat
    @Binding var currentUserInteractionCellID: String?
    
    let password: Password
    let deletionCallback: () -> Void
    
    var body: some View {
        PasswordListItem(password: password)
            .swipeCell(id: self.password.id, cellWidth: availableWidth, leadingSideGroup: [], trailingSideGroup: rightGroup(deletionCallback: deletionCallback), currentUserInteractionCellID: $currentUserInteractionCellID, settings: SwipeCellSettings())
                  .onTapGesture {
                      self.currentUserInteractionCellID = password.id
                  }
    }
    
    private func rightGroup(deletionCallback: @escaping () -> ()) -> [SwipeCellActionItem] {
        return [
            SwipeCellActionItem(buttonView: {
                self.trashView(swipeOut: false)
            }, swipeOutButtonView: {
                self.trashView(swipeOut: true)
            }, backgroundColor: .red, swipeOutAction: true, swipeOutHapticFeedbackType: .warning, swipeOutIsDestructive: true) {
                deletionCallback()
            }
        ]
    }
        
    private func trashView(swipeOut: Bool)->AnyView {
        VStack(spacing: 3)  {
            Image(systemName: "trash").font(.system(size: swipeOut ? 28 : 22)).foregroundColor(.white)
            Text(LocalizedStringKey("all_passwords_item_delete")).fixedSize().font(.system(size: swipeOut ? 16 : 12)).foregroundColor(.white)
        }.frame(maxHeight: 80).animation(.default).cornerRadius(12).castToAnyView()
        
    }
}


struct PasswordListItem: View {
    let password: Password
    
    var body: some View {
        Card("", shadowRadius: CGFloat(4)) {
            VStack(alignment: .leading, spacing: 0) {
                Text(password.name)
                    .font(.title)
                    .foregroundColor(Constants.CONTRAST_COLOR)
                    .frame(maxWidth: .infinity, alignment: .leading)
               
                Text(password.link)
                    .font(.subheadline)
                    .italic()
                    .foregroundColor(Constants.CONTRAST_COLOR)
                    .frame(maxWidth: .infinity, alignment: .leading)
                
                Text(password.description)
                    .font(.callout)
                    .foregroundColor(.gray)
                    .frame(maxWidth: .infinity, alignment: .leading)
                
                Text(password.timeFormatted)
                    .font(.callout)
                    .italic()
                    .foregroundColor(.gray)
                    .frame(maxWidth: .infinity, alignment: .trailing)
            }.padding(5)
        }
        .padding(5)
        .onTapGesture {
            Router.shared.toDetails(of: password)
        }
    }
}

/*
struct PasswordsItem_Previews: PreviewProvider {
    static var previews: some View {
        PasswordListItem(password: FakePasswordsUseCase().getPasswordBy(id: "p1")!)
    }
}
*/

struct PasswordsListScreen_Previews: PreviewProvider {
    static var previews: some View {
        PasswordsListScreen(viewModel: PasswordsViewModel(passwordsUseCase: FakePasswordsUseCase(capacity: 2)))
    }
}
