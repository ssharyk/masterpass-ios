import SwiftUI
import Component

struct SinglePasswordScreen: View {
    @ObservedObject var viewModel: PasswordsViewModel 
    
    let password: Password
    private var isEditMode: Bool { password.id == Password.NO_ID }
    
    @State var title: String
    @State var link: String
    @State var description: String
    
    @State var plainPassword: String
    @State var length: Float
    @State var optionDigits = true
    @State var optionSpecs = true
    @State var optionTwoCases = true
    
    @State private var showErrorToast = false
    @State private var errorMessage = ""
    
    init(viewModel: PasswordsViewModel, password: Password) {
        self.viewModel = viewModel
        self.password = password
        
        title = password.name
        link = password.link
        description = password.description
        plainPassword = password.plainPassword
        length = Float(max(PasswordsViewModel.MIN_PASSWORD_LENGTH,  password.plainPassword.count))
    }
    
    var body: some View {
        VStack {
            Toolbar(LocalizedStringKey(
                isEditMode ? "details_toolbar_create" : "details_toolbar_edit"), onBackPressed: {
                Router.shared.up()
            }) {
                Image(systemName: "cross.circle")
                    .imageScale(.large)
                    .onTapGesture {
                        let savingResult = viewModel.save(
                            id: password.id, name: title, link: link, description: description, password: plainPassword
                        )
                        if savingResult.isEmpty {
                            showErrorToast = false
                            Router.shared.up()
                            return
                        }
                        
                        showErrorToast = true
                        let errorField = savingResult[0]
                        switch(errorField) {
                            case .resourceName:
                                errorMessage = "details_validation_name"
                            case .resourceLink:
                                errorMessage = "details_validation_resource"
                            case .plainPassword:
                                errorMessage = "details_validation_password"
                            case .cipher:
                                errorMessage = "details_validation_cipher"
                            case .store:
                                errorMessage = "details_validation_store"
                        }
                    }
            }
            
            ScrollView {
                Card(LocalizedStringKey("details_field_general"), shadowRadius: Constants.SHADOW) {
                    VStack {
                        TextField(LocalizedStringKey("details_field_name"), text: $title) {}
                            .padding()
                            .border(Constants.BORDER_COLOR, width: CGFloat(3))
                        
                        TextField(LocalizedStringKey("details_field_link"), text: $link) {}
                            .padding()
                            .border(Constants.BORDER_COLOR, width: CGFloat(3))
                        
                        TextField(LocalizedStringKey("details_field_description"), text: $description) {}
                            .padding()
                            .border(Constants.BORDER_COLOR, width: CGFloat(3))
                    }.padding(10)
                }.padding()
                
                Spacer()
                
                Card(LocalizedStringKey("details_field_password"), shadowRadius: Constants.SHADOW) {
                    VStack {
                        TextField(LocalizedStringKey("details_field_password"), text: $plainPassword) {}
                            .padding()
                            .border(Constants.BORDER_COLOR, width: CGFloat(3))
                        
                        Text(LocalizedStringKey("or"))
                            .foregroundColor(Constants.FOREGROUND_COLOR)
                            .font(.body)
                        
                        Slider(value: $length, in: 8...32, step: 1) {
                           Text(LocalizedStringKey("details_field_password_length"))
                        }
                        .accentColor(Constants.FOREGROUND_COLOR)
                        
                        Toggle(LocalizedStringKey("details_field_option_digits"), isOn: $optionDigits)
                            .font(.callout)
                            .tintColor(Constants.FOREGROUND_COLOR)
                        
                        Toggle(LocalizedStringKey("details_field_option_specs"), isOn: $optionSpecs)
                            .font(.callout)
                            .tintColor(Constants.FOREGROUND_COLOR)
                        
                        Toggle(LocalizedStringKey("details_field_option_two_cases"), isOn: $optionTwoCases)
                            .font(.callout)
                            .tintColor(Constants.FOREGROUND_COLOR)
                        
                        
                        Button(LocalizedStringKey("details_field_button_generate"), action: {
                            plainPassword = viewModel.generateNew(length: Int(length), withDigits: optionDigits, withSpecs: optionSpecs, withTwoCases: optionTwoCases)
                        })
                            .foregroundColor(Constants.FOREGROUND_COLOR)
                            .padding()
                    }.padding(10)
                }.padding()
            }
            
        }
        .toast(message: LocalizedStringKey(errorMessage), isShowing: $showErrorToast, duration: Toast.short)
    }
}



struct SinglePasswordScreen_Previews: PreviewProvider {
    private static let vm = PasswordsViewModel(passwordsUseCase: FakePasswordsUseCase(capacity: 2))
    
    static var previews: some View {
        SinglePasswordScreen(viewModel: vm, password: vm["p1"])
    }
}
