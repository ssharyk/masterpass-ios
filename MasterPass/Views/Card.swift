import SwiftUI

struct Card<Content: View>: View {
    
    var title: String?
    var titleKey: LocalizedStringKey?
    
    var shadowRadius: CGFloat = CGFloat(10)
    let content: () -> Content
    
    init(_ title: String, shadowRadius: CGFloat, content: @escaping () -> Content) {
        self.title = title
        self.shadowRadius = shadowRadius
        self.content = content
    }
    
    init(_ titleKey: LocalizedStringKey, shadowRadius: CGFloat, content: @escaping () -> Content) {
        self.init("", shadowRadius: shadowRadius, content: content)
        self.titleKey = titleKey
    }
    
    var body: some View {
        ZStack(alignment: .leading) {
            RoundedRectangle(cornerRadius: 10)
                .fill(Constants.BACKGROUND_COLOR)
                .shadow(radius: shadowRadius)
                
            VStack {
                if let titleTv = createTitleTextView() {
                    titleTv
                        .foregroundColor(Constants.FOREGROUND_COLOR)
                        .font(.title2)
                        .padding(.top, 15)
                }
                
                content()
                    .padding(5)
            }
        }
    }
    
    private func createTitleTextView() -> Text? {
        if titleKey != nil { return Text(titleKey!) }
        guard title != nil && !title!.isEmpty else { return nil }
        return Text(title!)
    }
}




struct CardView_Previews: PreviewProvider {
    static var previews: some View {
        Card("", shadowRadius: CGFloat(10)) {
            Text("Hello World!")
        }
    }
}
