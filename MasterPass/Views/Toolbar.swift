import SwiftUI

struct Toolbar<Content: View>: View {
    
    var title: String?
    var titleKey: LocalizedStringKey?
    let onBackPressed: (() -> ())?
    let trailingIcon: () -> Content
    
    init(_ title: String, onBackPressed: (() -> ())?, trailingIcon: @escaping () -> Content) {
        self.title = title
        self.onBackPressed = onBackPressed
        self.trailingIcon = trailingIcon
    }
    
    init(_ titleKey: LocalizedStringKey, onBackPressed: (() -> ())?, trailingIcon: @escaping () -> Content) {
        self.init("", onBackPressed: onBackPressed, trailingIcon: trailingIcon)
        self.titleKey = titleKey
    }
    
    var body: some View {
        HStack {
            Spacer().frame(width: 24, height: 1, alignment: .leading)
            if (onBackPressed != nil) {
                Image(systemName: "chevron.left").frame(width: Constants.TOOLBAR_SIZE, height: Constants.TOOLBAR_SIZE, alignment: .center)
                    .foregroundColor(Constants.FOREGROUND_COLOR)
                    .onTapGesture { onBackPressed!() }
            }
            
            titleTextView()
            
            Spacer()
            
            trailingIcon()
                .padding()
                .foregroundColor(Constants.FOREGROUND_COLOR)
        }
        .frame(maxWidth: .infinity)
    }
    
    private func titleTextView() -> Text {
        let titleTv = titleKey == nil ? Text(title ?? "") : Text(titleKey!)
        return titleTv
            .font(.title)
            .foregroundColor(Constants.FOREGROUND_COLOR)
    }
}



struct Toolbar_Previews: PreviewProvider {
    static var previews: some View {
        Toolbar("Some title", onBackPressed: {}, trailingIcon: {
            Text("Hello World!")
        })
    }
}
