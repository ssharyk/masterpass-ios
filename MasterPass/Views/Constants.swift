import CoreGraphics
import SwiftUI

struct Constants {
    static let TOOLBAR_SIZE: CGFloat = 45
    
    static let SHADOW = CGFloat(10)
    
    static let BACKGROUND_COLOR = Color("CardBackgroundColor")
    static let CONTRAST_COLOR = Color("ContrastColor")
    static let FOREGROUND_COLOR: Color = .purple
    static let PRIMARY_COLOR: Color = .purple
    static let BORDER_COLOR: Color = .purple
}
